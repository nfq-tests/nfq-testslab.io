import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { updateTask } from '../reducers/media';

class Body extends Component {
  static propTypes = {
    task: PropTypes.string
  }

  render() {
    return (
      <div>{this.props.task}</div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  console.log(111, 'render Comp2')
  return { task: state.media.task }
}


export default connect(mapStateToProps)(Body)
