import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Body extends Component {
  static propTypes = {
    updateTask: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = { isOpen: 'false' };;
    console.log(123, 'init');
  }

  componentDidMount() {
    console.log(123, 'mounted')
  }

  componentWillUnmount() {
    console.log(123, 'un-mounted')
  }

  onClickHandler = () => {
    console.log(this)
    this.setState(currentState => ({
      isOpen: 'true'
    }));
  }

  render() {
    console.log(123, 'render Comp1');
    return (
      <div>
        999, {this.state.isOpen}
        <button onClick={this.onClickHandler}>Set state</button>
        <button onClick={this.props.updateTask}>Set task</button>
      </div>
    )
  }
}
