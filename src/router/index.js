import React, { PureComponent } from 'react';
import { Switch, Route } from 'react-router-dom';
import Index from '../containers/Index';

/*const Router = () => {
  return (
    <Switch>
      <Route exact path="/" component={ Index } />
    </Switch>
  )
}*/

class Router extends PureComponent {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={ Index } />
      </Switch>
    );
  }
}
export default Router;
