import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { updateTask } from '../reducers/media';
import { withRouter } from 'react-router';
import Comp1 from '../components/Comp1';
import Comp2 from '../components/Comp2';

class Index extends Component {
  static propTypes = {
    updateTask: PropTypes.func
  }

  shouldComponentUpdate() {
    console.log('work ?');
    return true
  }

  constructor(props) {
    super(props);
  }

  render() {
    console.log(999, 'index page', this)
    return (
      <div>
        <Comp2/>
        <Comp1 updateTask={() => {
          this.props.updateTask('test' + Date.now())
        }}/>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {}
}


export default withRouter(connect(mapStateToProps, {
  updateTask
})(Index))

