import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import reducers from './index';
import { loadState, saveState } from './state';
import throttle from 'lodash/throttle';

const middleware = [ thunk ];
const persistedState = loadState();

if (process.env.NODE_ENV !== 'production') {
  middleware.push(createLogger());
}

const store = createStore(
  reducers,
  persistedState,
  applyMiddleware(...middleware)
);

store.subscribe(throttle(() => {
  saveState({
    media: store.getState().media
  })
}), 1e3)

export default store;