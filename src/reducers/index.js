import { combineReducers } from 'redux'
import { media } from './media'

const reducers = combineReducers({media})
export default reducers