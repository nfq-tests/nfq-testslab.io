export const Keys = {
  UPDATE_TASK: 'UPDATE_TASK'
}

const initialState = {
  task: 'demo'
}

export const media = (state = initialState, action) => {
  switch (action.type) {
    case Keys.UPDATE_TASK:
      return {...state , task: action.task}
    default: return state
  }
}

//action list
export const updateTask = task => (dispatch, getState) => {
  return dispatch({
    type: Keys.UPDATE_TASK,
    task: task
  })
}