export const loadState = (state) => {
  try{
    const rawState = localStorage.getItem('state');
    if (rawState === null) {
      return undefined;
    }
    return JSON.parse(rawState)
  } catch (err) {
    return undefined;
  }
}

export const saveState = (state) => {
  try{
    const rawState = JSON.stringify(state);
    localStorage.setItem('state', rawState);
  } catch (err) {
    // error
  }
}