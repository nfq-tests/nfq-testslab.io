import React from 'react';
import ReactDOM from 'react-dom';
import Router from './router';
import registerServiceWorker from './registerServiceWorker'
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import store from './reducers/store'

ReactDOM.render(
<Provider store={store}>
  <BrowserRouter>
    <Router/>
  </BrowserRouter>
</Provider>, document.getElementById('app'));
registerServiceWorker();
